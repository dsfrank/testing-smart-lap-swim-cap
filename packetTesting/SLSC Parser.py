

def main():
	
	f = open('packets.txt', 'r')
	
	packets = []
	
	for line in f:
		packets.append(line.strip().split(', '))
	
	
	f.close()
	#print len(packets)
	
	numberedPackets = [(e[0][119:125], e[0], e[1], e[2]) for e in packets]
	# print numberedPackets[0]
	numberedPackets = sorted(numberedPackets)
	
	print "num lines:"
	print len(numberedPackets)
	
	numDataPoints = int(numberedPackets[-1][0],16) - int(numberedPackets[0][0],16)
	print "data points:"
	print numDataPoints 

	baseline = int(numberedPackets[0][0], 16)

	packetTable = [ ['0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'] for i in range(numDataPoints)]
	
	
	print "baseline:"
	print baseline
	# print len(packetTable)
	
	for datapoint in numberedPackets[:-1]:
	
		packet = datapoint[1]
		timestamp = int(packet[12:20], 16)
		# print "timestamp: " + str(timestamp)
		
		#print packet
		
		# print datapoint
		sampleNum = int(datapoint[0], 16)
		
		dataGX = packet[20:36]
		# print "gyro X: " + dataGX
		
		dataGY = packet[36:52]
		# print "gyro Y: " + dataGY
		
		dataGZ = packet[52:63] + packet[65:70]
		# print "gyro Z: " + dataGZ
		
		dataAX = packet[70:86]
		# print "accel X: " + dataAX
		
		dataAY = packet[86:102]
		# print "accel Y: " + dataAY
		
		dataAZ = packet[102:118]
		# print "accel Z: " + dataAZ
		
		rssi = datapoint[2]
		#print rssi
		
		#write Gyro X data
		numInList = sampleNum - baseline
		while(numInList >= 0 and len(dataGX) > 0):
			
			packetTable[numInList][0] = str(int(dataGX[0:2], 16))	
			
			dataGX2 = int(dataGX[0:2], 16)
			if (dataGX2 > 127):
				dataGX2 = dataGX2 - 256
				
			packetTable[numInList][8] = str(dataGX2)	
			packetTable[numInList][6] = str(numInList)
			packetTable[numInList][7] = str(rssi)
			
			dataGX = dataGX[2:]
			numInList -= 1
			
		#write Gyro Y data
		numInList = sampleNum - baseline
		while(numInList >= 0 and len(dataGY) > 0):
			packetTable[numInList][1] = str(int(dataGY[0:2], 16))
			
			dataGY2 = int(dataGY[0:2], 16)
			if (dataGY2 > 127):
				dataGY2 = dataGY2 - 256
				
			packetTable[numInList][9] = str(dataGY2)	
			
			dataGY = dataGY[2:]
			numInList -= 1

		#write Gyro Z data
		numInList = sampleNum - baseline
		while(numInList >= 0 and len(dataGZ) > 0):
			packetTable[numInList][2] = str(int(dataGZ[0:2], 16))
			
			dataGZ2 = int(dataGZ[0:2], 16)
			if (dataGZ2 > 127):
				dataGZ2 = dataGZ2 - 256
				
			packetTable[numInList][10] = str(dataGZ2)	
			
			dataGZ = dataGZ[2:]
			numInList -= 1

		#write Accl X data
		numInList = sampleNum - baseline
		while(numInList >= 0 and len(dataAX) > 0):
			packetTable[numInList][3] = str(int(dataAX[0:2], 16))
			
			dataAX2 = int(dataAX[0:2], 16)
			if (dataAX2 > 127):
				dataAX2 = dataAX2 - 256
				
			packetTable[numInList][11] = str(dataAX2)	
			
			dataAX = dataAX[2:]
			numInList -= 1
			
		#write Accl Y data
		numInList = sampleNum - baseline
		while(numInList >= 0 and len(dataAY) > 0):
			packetTable[numInList][4] = str(int(dataAY[0:2], 16))
			
			dataAY2 = int(dataAY[0:2], 16)
			if (dataAY2 > 127):
				dataAY2 = dataAY2 - 256
				
			packetTable[numInList][12] = str(dataAY2)	
			
			dataAY = dataAY[2:]
			numInList -= 1	
			
		#write Accl Z data
		numInList = sampleNum - baseline
		while(numInList >= 0 and len(dataAZ) > 0):
			packetTable[numInList][5] = str(int(dataAZ[0:2], 16))
			
			dataAZ2 = int(dataAZ[0:2], 16)
			if (dataAZ2 > 127):
				dataAZ2 = dataAZ2 - 256
				
			packetTable[numInList][13] = str(dataAZ2)	
			
			dataAZ = dataAZ[2:]
			numInList -= 1	
			
			
		
			
	# for i in packetTable[:10]:
		# print ', '.join(i)
	
	
	
	fout = open('packets.csv', 'w')
	fout.write('GX, GY, GZ, AX, AY, AZ, ##, RSSI, GX 2sComp, GY 2sComp, GZ 2sComp, AX 2sComp, AY 2sComp, AZ 2sComp\n')
	for line in packetTable:
		fout.write(', '.join(line))
		fout.write('\n')

	
	fout.close()

main()