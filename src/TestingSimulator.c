#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>
#include <math.h>


// StartLapConstants
#define GX_START_THRESHOLD					  15
//#define POOL_START_THRESHOLD				  15		//
#define FLIP_TURN_THRESHOLD					  -11		//
#define BREATH_HIGH_THRESHOLD				  28		//
#define BREATH_LOW_THRESHOLD 				  -23		//
#define STROKE_INTERVAL						  10		//
#define STROKE_LOW_THRESHOLD				  -5		//
#define STROKE_HIGH_THRESHOLD				  5			//
#define POOL_STOP_THRESHOLD					  -20		//

#define POOL_START_INTERVAL					  20
#define HALLWAY_BREATH_THRESHOLD			  45


// ProObject Identifier
#define PROOBJECT_ID						  0xAC03



int main(void) {
	int csvSize = -1;
	//FILE *zFile;		// Do not use this variable if the data is parsed into a single file
	//FILE *xFile;		// Do not use this variable if the data is parsed into a single file
	FILE *lengthFile;
	FILE *dataFile;
	int lineSize = 128;
	char line[lineSize];

	// Variables for Data Processing
	static int halfSecondBufferX[5], threeSecondBufferX[30], halfSecondBufferGX[5], halfSecondBufferZ[5];
	static int oneSecondBufferX[10], oneSecondBufferZ[10], oneSecondBufferGX[10], oneSecondBufferY[10];
	static int threeSecondBufferZ[30], fiveSecondBufferZ[50];
	static int averageOverOneSecondX, averageOverOneSecondZ, averageOverOneSecondGX;
	static int twoSecondBufferY[20];
	static int x, z, gx, y;

	// Variables for Starts and Stops
	static uint8_t timesOverThreshold;
	static uint8_t stoppingThresholdCounter;
	static int bufferThreeSecondCounter, bufferFiveSecondCounter;
	bool inLap;
	int thresholdLoop;

	// Variables for Flip-turns
	static int bufferCounter;
	static uint8_t flipTurns;
	static uint8_t onlyOneFlip;
	static uint8_t currentMax;
	bool inFlip;

	// Variables for Breaths
	static uint8_t breathsSinceLastFlip;
	static uint8_t onlyOneBreath;
	static uint8_t previousZ;
	bool breathing;
	static uint8_t lapBreathCount;
	static uint8_t totalBreathCount;
	//static uint8_t strokes;

	static int lapCount;
	static int sessionCount;
	static float lapTime;
	static int startLap;
	static int endLap;
	bool startedFirstSwim;
	static float sessionTime;

	bool calibrating;
	static int twoSecondBufferX[20];
	//static uint32_t averageOverTwoSecondsX;
	static int maxThreeSecondAvgX;
	static int averageOverFiveSecondsX;
	static int tempAdder;
	static int startedSwimming;
	static int twoSecondBufferX2[20];
	static int averageOverTwoSecondsX;
	static int breathMinAvg;
	static int breathMin;
	static int tempZHighAvg;
	static int tempBreathMin;
	bool tempBreathMinHit;
	static int breathThreshold;
	static int stoppingThreshold;
	static int breathMinPoint;
	static int oneSecondMax;
	static int oneSecondMin;
	static int peakInData;
	static int nonPeakCounter;
	static int breathFound;
	static int averageOverHalfSecondX;
	static int averageOverHalfSecondGX;
	static int strokeLow;
	static int strokeLowCounter;
	static int strokeLowFound;
	static int strokeHigh;
	static int strokeHighCounter;
	static int strokeHighFound;
	static int bufferCounterHalf;
	static int lapStrokeCount;
	static int strokeHighCountdown;
	static int strokeLowCountdown;
	static int breathHighCounter;
	static int flipThresholdCounter;
	static int totalStrokeCount;
	static int averageOverThreeSecondsX;
	static int j;
	static int minZ;
	static int startValueOneSecondZ;
	static int breathMinLocation;
	static int flipTurnHit;
	bool belowStartValue;
	static int breathHighThreshold;
	static int tempBreathMinLocation;
	bool highBreathFoundFirst;
	static int minGX;
	static int averageOverTwoSecondsY;
	static int minY;
	static int averageOverOneSecondY;
	static int inFlipCounter;
	static int poolStart;
	static int endOfPushOff;
	static int breathLowCounter;
	static int onlyOneHighBreath;
	static int onlyOneLowBreath;
	static int lastZ;
	static int lastX;
	static int lastGX;
	static int lastY;
	static int dataLossCounterZ;
	static int dataLossCounterX;
	static int dataLossCounterGX;
	static int dataLossCounterY;
	static int dataLossSpotZ;
	static int dataLossSpotX;
	static int dataLossSpotGX;
	static int dataLossSpotY;
	static int intDataLossOffsetZ;
	static int intDataLossOffsetX;
	static int intDataLossOffsetGX;
	static int intDataLossOffsetY;
	static float floatDataLossOffsetZ;
	static float floatDataLossOffsetX;
	static float floatDataLossOffsetGX;
	static float floatDataLossOffsetY;
	static float decimalDataLossOffsetZ;
	static float decimalDataLossOffsetX;
	static float decimalDataLossOffsetGX;
	static float decimalDataLossOffsetY;
	static int decimalCounterZ;
	static int decimalCounterX;
	static int decimalCounterGX;
	static int decimalCounterY;
	static int startValueZ;
	static int startValueX;
	static int startValueGX;
	static int startValueY;
	static int accuDataLossZ;
	static int accuDataLossX;
	static int accuDataLossGX;
	static int accuDataLossY;
	static int lastStroke;
	static int timeSinceLastStroke;
	static int dropInZ;
	static int averageOverHalfSecondZ;
	static int prevHalfSecondZ;




	// Initializing variables
	bufferCounter = 0;
	flipTurns = 0;
	onlyOneFlip = 0;
	onlyOneBreath = 0;
	currentMax = 0;
	inFlip = 0;
	breathing = 0;
	breathsSinceLastFlip = 0;
	previousZ = 0;
	bufferThreeSecondCounter = 0;
	inLap = 0;
	timesOverThreshold = 0;
	stoppingThresholdCounter = 0;
	bufferFiveSecondCounter = 0;
	x = 0;
	z = 0;
	gx = 0;
	y = 0;
	memset(halfSecondBufferX, 0, 5);
	memset(oneSecondBufferX,0,10);
	memset(oneSecondBufferZ,0,10);
	memset(threeSecondBufferZ,0,30);
	memset(fiveSecondBufferZ,0,50);
	memset(oneSecondBufferGX,0,10);
	memset(halfSecondBufferGX, 0, 5);
	memset(halfSecondBufferZ, 0, 5);
	lapCount = 0;
	lapBreathCount = 0;
	totalBreathCount = 0;
	sessionCount = 1;
	lapTime = 0.00;
	startLap = 0;
	endLap = 0;
	startedFirstSwim = 0;
	sessionTime = 0.0;
	//
	calibrating = 0;		// Set to 1 if calibrating, or set to 0 if running the simulator
	//
	memset(twoSecondBufferX, 0, 20);
	maxThreeSecondAvgX = -127;
	tempAdder = 0;
	startedSwimming = 0;
	memset(twoSecondBufferX2, 0, 20);
	breathMin = 127;
	tempBreathMinHit = 0;
	breathMinPoint = 3;
	nonPeakCounter = 0;
	bufferCounterHalf = 0;
	lapStrokeCount = 0;
	strokeHighCounter = 0;
	strokeLowCounter = 0;
	strokeHighCountdown = STROKE_INTERVAL * 1.5;
	strokeLowCountdown = STROKE_INTERVAL * 1.5;
	breathHighCounter = 0;
	flipThresholdCounter = 0;
	totalStrokeCount = 0;
	memset(threeSecondBufferX, 0, 30);
	j = 0;
	averageOverThreeSecondsX = 0;
	minZ = 127;
	startValueOneSecondZ = 0;
	breathMinLocation = 0;
	flipTurnHit = 0;
	belowStartValue = 0;
	breathHighThreshold = -127;
	tempBreathMinLocation = 0;
	highBreathFoundFirst = 1;
	minGX = 127;
	memset(twoSecondBufferY, 0, 20);
	averageOverTwoSecondsY = 0;
	minY = 127;
	averageOverOneSecondY = 0;
	memset(oneSecondBufferY, 0, 10);
	inFlipCounter = 0;
	poolStart = 0;
	endOfPushOff = 0;
	breathLowCounter = 0;
	onlyOneHighBreath = 0;
	onlyOneLowBreath = 0;
	lastZ = 0;
	lastX = 0;
	lastGX = 0;
	lastY = 0;
	dataLossCounterZ = 0;
	dataLossCounterX = 0;
	dataLossCounterGX = 0;
	dataLossCounterY = 0;
	dataLossSpotZ = 0;
	dataLossSpotX = 0;
	dataLossSpotGX = 0;
	dataLossSpotY = 0;
	intDataLossOffsetZ = 0;
	floatDataLossOffsetZ = 0;
	decimalDataLossOffsetZ = 0;
	decimalCounterZ = 0;
	startValueZ = 0;
	accuDataLossZ = 0;
	intDataLossOffsetZ = 0;
	intDataLossOffsetX = 0;
	intDataLossOffsetGX = 0;
	intDataLossOffsetY = 0;
	floatDataLossOffsetZ = 0;
	floatDataLossOffsetX = 0;
	floatDataLossOffsetGX = 0;
	floatDataLossOffsetY = 0;
	decimalDataLossOffsetZ = 0;
	decimalDataLossOffsetX = 0;
	decimalDataLossOffsetGX = 0;
	decimalDataLossOffsetY = 0;
	decimalCounterZ = 0;
	decimalCounterX = 0;
	decimalCounterGX = 0;
	decimalCounterY = 0;
	startValueZ = 0;
	startValueX = 0;
	startValueGX = 0;
	startValueY = 0;
	accuDataLossZ = 0;
	accuDataLossX = 0;
	accuDataLossGX = 0;
	accuDataLossY = 0;
	lastStroke = 0;
	timeSinceLastStroke = 0;
	dropInZ = 0;
	averageOverHalfSecondZ = 0;
	prevHalfSecondZ = 127;



	// Use this code if the data is parsed into a single file

	lengthFile = fopen("data.csv", "r");


	// Use this code if the data is parsed into x acceleration data
	// and z acceleration data separately

	//lengthFile = fopen("dataZ.csv", "r");






	if (lengthFile == NULL) {
		perror("Error opening z data file");
		return (-1);
	}
	while (fgets(line, lineSize, lengthFile) != NULL) {
		csvSize++;
	}
	fclose(lengthFile);



	if (csvSize > 0) {
		int counter = 0;
		int firstLine = 1;
		int zAcceleration[csvSize];
		int xAcceleration[csvSize];
		int xGyro[csvSize];
		int yAcceleration[csvSize];
		int zTemp[100];
		int zData = 0;
		//int xData = 0;    // Do not use this variable if the data is parsed into a single file
		char line[100];		//edit
		char *lineHelp;
		int i;
		memset(zAcceleration, 0, csvSize);
		memset(xAcceleration, 0, csvSize);
		memset(xGyro, 0, csvSize);
		memset(yAcceleration, 0, csvSize);
		int columnCounter = 0;






		// Use this code if the data is parsed into a single file

		dataFile = fopen("data.csv", "r");
		while (fgets(line, 100, dataFile) != NULL) {
			if (firstLine) {
				firstLine = 0;
			} else {
				lineHelp = strtok(line, ",");
				while (lineHelp != NULL) {
					int a = atoi(lineHelp);
					if (columnCounter == 3) {
						if (a > 127) {
							a = a - 256;
						}
						xAcceleration[counter] = a;
					} else if (columnCounter == 5) {
						if (a > 127) {
							a = a - 256;
						}
						zAcceleration[counter] = a;
						zData = a;
					} else if (columnCounter == 0) {
						if (a > 127) {
							a = a - 256;
						}
						xGyro[counter] = a;
					} else if (columnCounter == 4) {
						if (a > 127) {
							a = a -256;
						}
						yAcceleration[counter] = a;
					}
					lineHelp = strtok(NULL, ",");
					columnCounter++;
				}
				if (counter < 100) {
					zTemp[counter] = zData;
				}
				counter++;
				columnCounter = 0;
			}
		}
		fclose(dataFile);





		// Use this code if the data is parsed into x acceleration data
		// and z acceleration data separately
		/*
		zFile = fopen("dataZ.csv", "r");
		while (fgets(line, 100, zFile) != NULL) {
			if (firstLine) {
				firstLine = 0;
			} else {
				lineHelp = strtok(line, ",");
				while (lineHelp != NULL) {
					int a = atoi(lineHelp);
					if (a > 127) {
						a = a - 256;
					}
					zData = a;
					lineHelp = strtok(NULL, ",");
				}
				if (counter < 100) {
					zTemp[counter] = zData;
				}
				zAcceleration[counter] = zData;
				counter++;
			}
		}
		fclose(zFile);

		counter = 0;
		firstLine = 1;


		xFile = fopen("dataX.csv", "r");
		while (fgets(line, 100, xFile) != NULL) {
			if (firstLine) {
				firstLine = 0;
			} else {
				lineHelp = strtok(line, ",");
				while (lineHelp != NULL) {
					int a = atoi(lineHelp);
					if (a > 127) {
						a = a - 256;
					}
					xData = a;
					lineHelp = strtok(NULL, ",");
				}
				xAcceleration[counter] = xData;
				counter++;
			}
		}
		fclose(xFile);
	*/







		// Fixes a small bug that alters the first few values
		// of zAcceleration when dataX.csv is larger than dataZ.csv
		for (i = 0; i < 100; i++) {
			zAcceleration[i] = zTemp[i];
		}
		i = 0;



		for (i = 1; i < csvSize; i++) {
			// Fix Z
			if (zAcceleration[i] == 0) {
				if (lastZ == 0) {
					lastZ = zAcceleration[i-1];
					dataLossCounterZ++;
					dataLossSpotZ = i;
				} else {
					dataLossCounterZ++;
				}
			}
			if ((dataLossCounterZ > 0) && (zAcceleration[i] != 0)) {
				intDataLossOffsetZ = abs((zAcceleration[i] - lastZ)/dataLossCounterZ);
				floatDataLossOffsetZ = fabs(((float)zAcceleration[i] - (float)lastZ)/dataLossCounterZ);
				decimalDataLossOffsetZ = fabs(floatDataLossOffsetZ - (float)intDataLossOffsetZ);
				if (decimalDataLossOffsetZ > 0.5) {
					startValueZ = 1;
				} else if (decimalDataLossOffsetZ == 0) {
					startValueZ = 0;
				} else {
					startValueZ = 1/decimalDataLossOffsetZ;
				}
				decimalCounterZ = startValueZ;
				/*printf("z = %d, last z = %d, at %d\ndataLossCounterZ = %d\n", zAcceleration[i], lastZ, i, dataLossCounterZ);
				printf("int = %d, float = %.3f, decimal = %.3f\n\n", intDataLossOffsetZ, floatDataLossOffsetZ, decimalDataLossOffsetZ);
				*/
				for (j = 0; j < dataLossCounterZ; j++) {
					if ((decimalCounterZ == 1) && (startValueZ > 0)) {
						accuDataLossZ++;
						decimalCounterZ = startValueZ;
					} else if (startValueZ > 0) {
						decimalCounterZ--;
					}
					if (lastZ > zAcceleration[i]) {
						zAcceleration[j + dataLossSpotZ] = (lastZ + (j * ((zAcceleration[i] - lastZ) / dataLossCounterZ)) - accuDataLossZ);
					} else {
						zAcceleration[j + dataLossSpotZ] = (lastZ + (j * ((zAcceleration[i] - lastZ) / dataLossCounterZ)) + accuDataLossZ);
					}
				}
				dataLossCounterZ = 0;
				dataLossSpotZ = 0;
				lastZ = 0;
				accuDataLossZ = 0;
			}




			//Fix X
			if (xAcceleration[i] == 0) {
				if (lastX == 0) {
					lastX = xAcceleration[i-1];
					dataLossCounterX++;
					dataLossSpotX = i;
				} else {
					dataLossCounterX++;
				}
			}
			if ((dataLossCounterX > 0) && (xAcceleration[i] != 0)) {
				intDataLossOffsetX = abs((xAcceleration[i] - lastX)/dataLossCounterX);
				floatDataLossOffsetX = fabs(((float)xAcceleration[i] - (float)lastX)/(float)dataLossCounterX);
				decimalDataLossOffsetX = fabs(floatDataLossOffsetX - (float)intDataLossOffsetX);
				if (decimalDataLossOffsetX > 0.5) {
					startValueX = 1;
				} else if (decimalDataLossOffsetX == 0) {
					startValueX = 0;
				} else {
					startValueX = 1/decimalDataLossOffsetX;
				}
				decimalCounterX = startValueX;
				/*printf("z = %d, last z = %d, at %d\ndataLossCounterZ = %d\n", xAcceleration[i], lastX, i, dataLossCounterX);
				printf("int = %d, float = %.3f, decimal = %.3f\n\n", intDataLossOffsetX, floatDataLossOffsetX, decimalDataLossOffsetX);
				*/
				for (j = 0; j < dataLossCounterX; j++) {
					if ((decimalCounterX == 1) && (startValueX > 0)) {
						accuDataLossX++;
						decimalCounterX = startValueX;
					} else if (startValueX > 0) {
						decimalCounterX--;
					}
					if (lastX > xAcceleration[i]) {
						xAcceleration[j + dataLossSpotX] = (lastX + (j * ((xAcceleration[i] - lastX) / dataLossCounterX)) - accuDataLossX);
					} else {
						xAcceleration[j + dataLossSpotX] = (lastX + (j * ((xAcceleration[i] - lastX) / dataLossCounterX)) + accuDataLossX);
					}
				}
				dataLossCounterX = 0;
				dataLossSpotX = 0;
				lastX = 0;
				accuDataLossX = 0;
			}






			//Fix GX
			if (xGyro[i] == 0) {
				if (lastGX == 0) {
					lastGX = xGyro[i-1];
					dataLossCounterGX++;
					dataLossSpotGX = i;
				} else {
					dataLossCounterGX++;
				}
			}
			if ((dataLossCounterGX > 0) && (xGyro[i] != 0)) {
				intDataLossOffsetGX = abs((xGyro[i] - lastGX)/dataLossCounterGX);
				floatDataLossOffsetGX = fabs(((float)xGyro[i] - (float)lastGX)/dataLossCounterGX);
				decimalDataLossOffsetGX = fabs(floatDataLossOffsetGX - (float)intDataLossOffsetGX);
				if (decimalDataLossOffsetGX > 0.5) {
					startValueGX = 1;
				} else if (decimalDataLossOffsetGX == 0) {
					startValueGX = 0;
				} else {
					startValueGX = 1/decimalDataLossOffsetGX;
				}
				decimalCounterGX = startValueGX;
				/*printf("GX = %d, last GX = %d, at %d\ndataLossCounterGX = %d\n", GXAcceleration[i], lastGX, i, dataLossCounterGX);
				printf("int = %d, float = %.3f, decimal = %.3f\n\n", intDataLossOffsetGX, floatDataLossOffsetGX, decimalDataLossOffsetGX);
				*/
				for (j = 0; j < dataLossCounterGX; j++) {
					if ((decimalCounterGX == 1) && (startValueGX > 0)) {
						accuDataLossGX++;
						decimalCounterGX = startValueGX;
					} else if (startValueGX > 0) {
						decimalCounterGX--;
					}
					if (lastGX > xGyro[i]) {
						xGyro[j + dataLossSpotGX] = (lastGX + (j * ((xGyro[i] - lastGX) / dataLossCounterGX)) - accuDataLossGX);
					} else {
						xGyro[j + dataLossSpotGX] = (lastGX + (j * ((xGyro[i] - lastGX) / dataLossCounterGX)) + accuDataLossGX);
					}
				}
				dataLossCounterGX = 0;
				dataLossSpotGX = 0;
				lastGX = 0;
				accuDataLossGX = 0;
			}







			// Fix Y
			if (yAcceleration[i] == 0) {
				if (lastY == 0) {
					lastY = yAcceleration[i-1];
					dataLossCounterY++;
					dataLossSpotY = i;
				} else {
					dataLossCounterY++;
				}
			}
			if ((dataLossCounterY > 0) && (yAcceleration[i] != 0)) {
				intDataLossOffsetY = abs((yAcceleration[i] - lastY)/dataLossCounterY);
				floatDataLossOffsetY = fabs(((float)yAcceleration[i] - (float)lastY)/dataLossCounterY);
				decimalDataLossOffsetY = fabs(floatDataLossOffsetY - (float)intDataLossOffsetY);
				if (decimalDataLossOffsetY > 0.5) {
					startValueY = 1;
				} else if (decimalDataLossOffsetY == 0) {
					startValueY = 0;
				} else {
					startValueY = 1/decimalDataLossOffsetY;
				}
				decimalCounterY = startValueY;
				/*printf("Y = %d, last Y = %d, at %d\ndataLossCounterY = %d\n", YAcceleration[i], lastY, i, dataLossCounterY);
				printf("int = %d, float = %.3f, decimal = %.3f\n\n", intDataLossOffsetY, floatDataLossOffsetY, decimalDataLossOffsetY);
				*/
				for (j = 0; j < dataLossCounterY; j++) {
					if ((decimalCounterY == 1) && (startValueY > 0)) {
						accuDataLossY++;
						decimalCounterY = startValueY;
					} else if (startValueY > 0) {
						decimalCounterY--;
					}
					if (lastY > yAcceleration[i]) {
						yAcceleration[j + dataLossSpotY] = (lastY + (j * ((yAcceleration[i] - lastY) / dataLossCounterY)) - accuDataLossY);
					} else {
						yAcceleration[j + dataLossSpotY] = (lastY + (j * ((yAcceleration[i] - lastY) / dataLossCounterY)) + accuDataLossY);
					}
				}
				dataLossCounterY = 0;
				dataLossSpotY = 0;
				lastY = 0;
				accuDataLossY = 0;
			}
		}
		j = 0;

		/*for (i = 1160; i < 1185; i++) {
			printf("Z = %d at %d\n", xAcceleration[i], i + 1);
		}*/




		// Helps alleviate basic data loss
		for (i = 1; i < csvSize; i++) {
			if (zAcceleration[i] == 0) {
				zAcceleration[i] = zAcceleration[i-1];
			}
			if (xAcceleration[i] == 0) {
				xAcceleration[i] = xAcceleration[i-1];
			}
			if (xGyro[i] == 0) {
				xGyro[i] = xGyro[i-1];
			}
			if (yAcceleration[i] == 0) {
				yAcceleration[i] = yAcceleration[i-1];
			}
		}



		if (!calibrating) {

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Testing~Algorithm~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

			for (i = 0; i < csvSize; i++) {

				x = xAcceleration[i];
				z = zAcceleration[i];
				gx = xGyro[i];
				y = yAcceleration[i];





				/*if (i < 1000) {
					printf("i = %d, x = %d, z = %d\n", i, x, z);
				}*/

				//	prevents sensorTag from detecting another lap if one was already detected for 3 seconds
				if (onlyOneFlip != 0) {
					onlyOneFlip--;
				}

				if (onlyOneBreath != 0) {
					onlyOneBreath--;
				}

				if (onlyOneHighBreath != 0) {
					onlyOneHighBreath--;
				}

				if (onlyOneLowBreath != 0) {
					onlyOneLowBreath--;
				}

				if (inFlip) {
					inFlipCounter++;
				}

				if (dropInZ > 0) {
					dropInZ--;
				}




				//	sets a buffer of 10 samples of x and z data
				oneSecondBufferX[bufferCounter] = x;
				oneSecondBufferZ[bufferCounter] = z;
				oneSecondBufferGX[bufferCounter]  = gx;
				oneSecondBufferY[bufferCounter] = y;
				bufferCounter = (bufferCounter + 1) % 10;
				halfSecondBufferX[bufferCounterHalf] = x;
				halfSecondBufferGX[bufferCounterHalf] = gx;
				halfSecondBufferZ[bufferCounterHalf] = z;
				bufferCounterHalf = (bufferCounterHalf + 1) % 5;


				//	calculates the averages over 10 intervals for 1 second of x and z data
				averageOverHalfSecondX = (halfSecondBufferX[0] + halfSecondBufferX[1] + halfSecondBufferX[2] + halfSecondBufferX[3] + halfSecondBufferX[4])/5;

				averageOverHalfSecondGX = (halfSecondBufferGX[0] + halfSecondBufferGX[1] + halfSecondBufferGX[2] + halfSecondBufferGX[3] + halfSecondBufferGX[4])/5;

				averageOverHalfSecondZ = (halfSecondBufferZ[0] + halfSecondBufferZ[1] + halfSecondBufferZ[2] + halfSecondBufferZ[3] + halfSecondBufferZ[4])/5;

				averageOverOneSecondX = (oneSecondBufferX[0] + oneSecondBufferX[1] + oneSecondBufferX[2] + oneSecondBufferX[3] + oneSecondBufferX[4] +
						oneSecondBufferX[5] + oneSecondBufferX[6] + oneSecondBufferX[7] + oneSecondBufferX[8] +  + oneSecondBufferX[9])/10;

				averageOverOneSecondZ = (oneSecondBufferZ[0] + oneSecondBufferZ[1] + oneSecondBufferZ[2] + oneSecondBufferZ[3] + oneSecondBufferZ[4] +
						oneSecondBufferZ[5] + oneSecondBufferZ[6] + oneSecondBufferZ[7] + oneSecondBufferZ[8] +  + oneSecondBufferZ[9])/10;

				averageOverOneSecondGX = (oneSecondBufferGX[0] + oneSecondBufferGX[1] + oneSecondBufferGX[2] + oneSecondBufferGX[3] + oneSecondBufferGX[4] +
						oneSecondBufferGX[5] + oneSecondBufferGX[6] + oneSecondBufferGX[7] + oneSecondBufferGX[8] +  + oneSecondBufferGX[9])/10;

				averageOverOneSecondY = (oneSecondBufferY[0] + oneSecondBufferY[1] + oneSecondBufferY[2] + oneSecondBufferY[3] + oneSecondBufferY[4] +
						oneSecondBufferY[5] + oneSecondBufferY[6] + oneSecondBufferY[7] + oneSecondBufferY[8] + oneSecondBufferY[9])/10;




				// creates a 3 second buffer of values of accelerometer z for starts calculations
	/* x */		threeSecondBufferZ[bufferThreeSecondCounter] = x;
				bufferThreeSecondCounter = (bufferThreeSecondCounter + 1) % 30;

				//	creates a 5 second buffer of average accelerometer z values for stops calculation
				fiveSecondBufferZ[bufferFiveSecondCounter] = averageOverOneSecondZ;
				bufferFiveSecondCounter = (bufferFiveSecondCounter + 1) % 50;






				//printf("inLap = %d, i = %d\n", inLap, i);

				if (!inLap)
				{


					if (averageOverHalfSecondGX > GX_START_THRESHOLD) {
						inLap = 1;
						printf("\n~~~~~~~~~~~~~~%d~~~~~~~~~~~~~\n", i);
						printf("Swimming Session %d:\n", sessionCount);
						sessionCount++;
						startLap = i - 15;     // -15 to be at the beginning of the flip turn
						startedFirstSwim = 1;
						onlyOneFlip = 100;
						inFlip = 1;
						printf(" In Flip, %d\n", i);
					}




					/*timesOverThreshold = 0;
					//	counts the number of times accelerometer Z is over the threshold
					for (thresholdLoop = 0; thresholdLoop <= 29; thresholdLoop++)
					{
						if (threeSecondBufferZ[thresholdLoop] >= POOL_START_THRESHOLD)
						{
							timesOverThreshold++;
						}
					}
					//	sets to swimming if z acceleration if above threshold for 2 seconds out of 3
					if ((timesOverThreshold >= POOL_START_INTERVAL) && (inLap != 1))
					{
						inLap = 1;
						printf("\n~~~~~~~~~~~~~~%d~~~~~~~~~~~~~\n", i);
						printf("Swimming Session %d:\n", sessionCount);
						sessionCount++;
						startLap = i - 15;     // -15 to be at the beginning of the flip turn
						startedFirstSwim = 1;
						onlyOneFlip = 100;
						inFlip = 1;
						printf(" In Flip, %d\n", i);
					}*/
				}

				else
				{
					if (averageOverOneSecondGX < POOL_STOP_THRESHOLD) {
						stoppingThresholdCounter++;
					} else {
						stoppingThresholdCounter = 0;
					}
					if (stoppingThresholdCounter >= 10) {
						inLap = 0;
						stoppingThresholdCounter = 0;
						endLap = i;
						lapTime = (float)(endLap - startLap)/10;
						sessionTime += lapTime;

						if (flipTurns%2 == 1) {
							totalStrokeCount += lapStrokeCount;
							totalBreathCount += lapBreathCount;
							printf("\nLap %d:\nStrokes = %d\nBreaths = %d\nTime = %.1f seconds\n", lapCount, lapStrokeCount, lapBreathCount, lapTime);
							printf("(%d, %d)\n", startLap, endLap);
						}
						inLap = 0;
						// Fixes the issue where these would print at the start of data collection
						if ((startedFirstSwim) && (totalStrokeCount == 0) && (lapCount == 0) && (totalBreathCount == 0)) {
							sessionTime = 0;
							totalStrokeCount = 0;
							printf("~~~~~~~~~~~~~~Empty~~~~~~~~~~~~\n");
							//printf("i = %d\n", i);
						} else if (startedFirstSwim) {
							printf("\nNumber of laps = %d\nTotal strokes = %d", lapCount, totalStrokeCount);
							printf("\nTotal breaths = %d\nTotal time = %.1f seconds", totalBreathCount, sessionTime);
							//printf("\n%d", i);
							printf("\n~~~~~~~~~~~~~~%d~~~~~~~~~~~~~\n", i);
							sessionTime = 0;
							totalStrokeCount = 0;
						}
						lapCount = 0;
						breathsSinceLastFlip = 0;
						totalBreathCount = 0;
						flipTurns = 0;
						lapBreathCount = 0;
						lapStrokeCount = 0;
					}




					// Old stopping algorithm
					/*
					stoppingThresholdCounter = 0;
					//	counts the number of times average accelerometer Z is between the threshold
					for (thresholdLoop = 0; thresholdLoop <= 49; thresholdLoop++)
					{
						if ((fiveSecondBufferZ[thresholdLoop] < POOL_START_THRESHOLD) && (fiveSecondBufferZ[thresholdLoop] >= POOL_STOP_THRESHOLD))
						{
							stoppingThresholdCounter++;
						}
					}
					//	sets to not swimming if average z acceleration if between the thresholds 4.5 seconds out of 5
					if ((stoppingThresholdCounter >= 45) && (inLap == 1) && (onlyOneFlip == 0))
					{
						endLap = i - 30;
						lapTime = (float)(endLap - startLap)/10;
						sessionTime += lapTime;

						if (flipTurns%2 == 1) {
							totalStrokeCount += lapStrokeCount;
							totalBreathCount += lapBreathCount;
							printf("\nLap %d:\nStrokes = %d\nBreaths = %d\nTime = %.1f seconds\n", lapCount, lapStrokeCount, lapBreathCount, lapTime);
							printf("(%d, %d)\n", startLap, endLap);
						}
						inLap = 0;
						// Fixes the issue where these would print at the start of data collection
						if (startedFirstSwim) {
							printf("\nNumber of laps = %d\nTotal strokes = %d", lapCount, totalStrokeCount);
							printf("\nTotal breaths = %d\nTotal time = %.1f seconds", totalBreathCount, sessionTime);
							//printf("\n%d", i);
							printf("\n~~~~~~~~~~~~~~%d~~~~~~~~~~~~~\n", i);
							sessionTime = 0;
							totalStrokeCount = 0;
						}
						lapCount = 0;
						breathsSinceLastFlip = 0;
						totalBreathCount = 0;
						flipTurns = 0;
						lapBreathCount = 0;
						lapStrokeCount = 0;
					}
					*/
				}


				if (averageOverHalfSecondZ < 10) {
					if (averageOverHalfSecondZ > prevHalfSecondZ) {
						dropInZ = 5;
					}
				}

				/*if (i > 1700 && i < 1715) {
					printf("dropInZ = %d, at %d\n", dropInZ, i);
				}*/


				// 18 = 1 second delay, and .8 second offset from using Y axis
				if (inFlip && (inFlipCounter >= 18) && (averageOverHalfSecondX <= STROKE_HIGH_THRESHOLD)) {
					inFlip = 0;
					inFlipCounter = 0;
					printf(" Not In Flip, %d\n", i);
				}



				/*if (i > 810 && i < 825) {
					printf("gx = %d\n", averageOverOneSecondGX);
				}*/


				//	sets a lap state to ON if the average is over threshold.
				if ((averageOverOneSecondGX > GX_START_THRESHOLD) && (inFlip != 1) && (onlyOneFlip == 0) && (inLap))
				{
					currentMax = averageOverOneSecondX;
					flipThresholdCounter++;
					//printf("\n~Flip turn: %d\nCounter = %d\n", i, flipThresholdCounter);
				}






				//	sets a lap state to ON if the average is over threshold.
				/*if ((averageOverOneSecondX >= FLIP_TURN_THRESHOLD) && (inFlip != 1) && (onlyOneFlip == 0) && (inLap))
				{
					currentMax = averageOverOneSecondX;
					inFlip = 1;
					//printf("\n~Flip turn: %d\n", i);
				}*/


				// 	sets breathing state ON if the average is less than the previous and average Z acceleration is less than threshold
				if ((inLap) && (inFlip != 1) && (averageOverHalfSecondX >= BREATH_HIGH_THRESHOLD) && (onlyOneHighBreath == 0))
				{
					breathing = 1;
					previousZ = averageOverHalfSecondX;
					//printf("\nHigh Breath at %d\n\n", i);
					breathHighCounter++;
					//printf("BHCounter = %d, V = %d, at %d\n", breathHighCounter, averageOverHalfSecondX, i);
				}

				// 	sets breathing state ON if the average is less than the previous and average Z acceleration is less than threshold
				if ((inLap) && (inFlip != 1) && (averageOverHalfSecondX <= BREATH_LOW_THRESHOLD) && (onlyOneLowBreath == 0))
				{
					breathing = 1;
					previousZ = averageOverHalfSecondX;
					//printf("\nLow Breath at %d\n\n", i);
					breathLowCounter++;
				}


				// 	sets breathing state ON if the average is less than the previous and average Z acceleration is less than threshold
				/*if ((inFlip != 1) && (averageOverOneSecondZ <= BREATH_HIGH_THRESHOLD) && (previousZ > averageOverOneSecondZ) && (onlyOneBreath == 0))
				{
					breathing = 1;
					previousZ = averageOverOneSecondZ;
					//printf("Breath at %d\n", i);
				}*/

				// 	prevents breath algorithm from detecting a lap
				if ((inFlip == 1) && (breathing == 1))
				{
					breathing = 0;
				}

				/*if ((i > 1597) && (i < 1615)) {
					printf("x = %d, Counter = %d, breath = %d, oneBreath = %d\n", averageOverHalfSecondX, breathHighCounter, breathing, onlyOneBreath);
				}*/

				/*if ((i > 1395) && (i < 1445)) {
					printf("x = %d, Counter = %d, breath = %d, oneBreath = %d, i = %d\n", averageOverHalfSecondX, breathLowCounter, breathing, onlyOneBreath, i);
				}*/

				//	detects if the breath has been taken
				if ((dropInZ > 0) && (!inFlip) && (breathing == 1) && (inLap) && (breathHighCounter <= 1.5*STROKE_INTERVAL) && (breathHighCounter > 0) && (averageOverHalfSecondX < (STROKE_HIGH_THRESHOLD)) && (onlyOneHighBreath == 0))
				{
					breathsSinceLastFlip++; 	//	increment Breath Count
					onlyOneHighBreath = STROKE_INTERVAL;
					breathing = 0;
					lapBreathCount++;
					breathHighCounter = 0;
					printf("\n~Finished high breath: %d\n", i);
				}

				if ((averageOverHalfSecondX < STROKE_HIGH_THRESHOLD) && (inLap) && (breathHighCounter > 0)) {
					//printf("BreathHighCounter (%d) reset at %d\n", breathHighCounter, i);
					breathHighCounter = 0;
					breathing = 0;
				}

				//	detects if the breath has been taken
				if ((dropInZ > 0) && (!inFlip) && (breathing == 1) && (inLap) && (breathLowCounter <= STROKE_INTERVAL) && (breathLowCounter > 0) && (averageOverHalfSecondX > (STROKE_LOW_THRESHOLD)) && (onlyOneLowBreath == 0))
				{
					breathsSinceLastFlip++; 	//	increment Breath Count
					onlyOneLowBreath = STROKE_INTERVAL;
					breathing = 0;
					lapBreathCount++;
					breathLowCounter = 0;
					printf("\n~Finished low breath: %d\n", i);
				}

				if ((averageOverHalfSecondX > STROKE_LOW_THRESHOLD) && (inLap) && (breathLowCounter > 0)) {
					breathLowCounter = 0;
					breathing = 0;
				}



				//	detects if acceleration is increasing in the positive direction again, the breath has been taken
				/*if ((breathing == 1) && (previousZ < averageOverOneSecondZ) && (inLap))
				{
					breathsSinceLastFlip++; 	//	increment Breath Count
					onlyOneBreath = 10;
					breathing = 0;
					lapBreathCount++;
					//printf("\n~Finished taking breath: %d\n", i);
				}*/


				/*if (i > 2570 && i < 2600) {
					printf("Last stroke time = %d, last stroke = %d, onlyOneFlip = %d, i = %d\n", timeSinceLastStroke, lastStroke, onlyOneFlip, i);
				}*/

				if ((!inFlip) && (inLap) && (averageOverHalfSecondX > STROKE_HIGH_THRESHOLD) && (averageOverHalfSecondX < BREATH_HIGH_THRESHOLD)) {
					strokeHighCounter++;
					strokeHighCountdown--;
				} else if ((!inFlip) && (inLap) && (strokeHighCounter > 0) && (strokeHighCounter < 15) && (strokeHighCountdown <= 0)) {
					if ((lastStroke == 1) && (onlyOneFlip < (100 - STROKE_INTERVAL)) && (timeSinceLastStroke > STROKE_INTERVAL) && (timeSinceLastStroke < 2.5 * STROKE_INTERVAL)) {
						printf("`Low Stroke at %d\n", i - STROKE_INTERVAL);
						lapStrokeCount++;
					}
					lapStrokeCount++;
					timeSinceLastStroke = 0;
					strokeHighCounter = 0;
					printf("Stroke High at %d\n", i);
					lastStroke = 1;
					strokeHighCountdown = STROKE_INTERVAL;
				} else {
					strokeHighCounter = 0;
					strokeHighCountdown--;
				}

				if ((!inFlip) && (inLap) && (averageOverHalfSecondX < STROKE_LOW_THRESHOLD) /*&& (averageOverHalfSecondX > (STROKE_HIGH_THRESHOLD - STROKE_LOW_THRESHOLD) + STROKE_LOW_THRESHOLD)*/) {
					strokeLowCounter++;
					strokeLowCountdown--;
				} else if ((!inFlip) && (inLap) && (strokeLowCounter > 0) && (strokeLowCounter < 15) && (strokeLowCountdown <= 0)) {
					if ((lastStroke == 2) && (onlyOneFlip < (100 - STROKE_INTERVAL)) && (timeSinceLastStroke > STROKE_INTERVAL) && (timeSinceLastStroke < 2.5 * STROKE_INTERVAL)) {
						printf("`High Stroke at %d\n", i - STROKE_INTERVAL);
						lapStrokeCount++;
					}
					lapStrokeCount++;
					strokeLowCounter = 0;
					timeSinceLastStroke = 0;
					printf("Stroke Low at %d\n", i);
					lastStroke = 2;
					strokeLowCountdown = STROKE_INTERVAL;
				} else {
					strokeLowCounter = 0;
					strokeLowCountdown--;
				}









				/*if ((flipThresholdCounter >= (STROKE_INTERVAL * 2)) && (inLap) && (inFlip != 1) && (averageOverHalfSecondX < STROKE_HIGH_THRESHOLD)) {
					flipThresholdCounter = 0;
					onlyOneFlip = 100;
					//flipTurns++;
					printf("Flip turn? %d\n", flipTurns);
				}*/



				//if ((flipThresholdCounter >= (STROKE_INTERVAL * 2)) && (inLap) /*&& (inFlip != 1)*/ && (averageOverHalfSecondX < STROKE_HIGH_THRESHOLD)) {
				//if ((inLap) && (averageOverOneSecondY < FLIP_TURN_THRESHOLD) && (onlyOneFlip == 0)) {
				if ((inLap) && (onlyOneFlip == 0) && (flipThresholdCounter >= STROKE_INTERVAL/2)) {
					//printf("BH = %d\n", breathHighCounter);
					if (breathHighCounter > 0) {
						breathsSinceLastFlip++;
						onlyOneHighBreath = STROKE_INTERVAL;
						breathing = 0;
						lapBreathCount++;
						breathHighCounter = 0;
						printf("\n~Finished high breath: %d\n", i);
					} else if (breathLowCounter > 0) {
						breathsSinceLastFlip++;
						onlyOneLowBreath = STROKE_INTERVAL;
						breathing = 0;
						lapBreathCount++;
						breathLowCounter = 0;
						printf("\n~Finished low breath: %d\n", i);
					}
					flipThresholdCounter = 0;
					onlyOneFlip = 100;
					flipTurns++; 	//increases flipTurns
					lastStroke = 0;
					inFlip = 1;
					printf(" In Flip, %d\n", i);
					printf("Flip turns = %d, at %d\n", flipTurns, i);
					//printf("Flip Turn, -2 strokes\n");
					//lapStrokeCount -= 2;
					lapCount = (flipTurns + 1) / 2;
					if (flipTurns%2 == 0) {
						endLap = i + 20;  // +20 since the flip turn is read at the beginning
						lapTime = (float)(endLap - startLap)/10;
						sessionTime += lapTime;
						totalStrokeCount += lapStrokeCount;
						totalBreathCount += lapBreathCount;
						printf("\nLap %d:\nStrokes = %d\nBreaths = %d\nTime = %.1f seconds\n", lapCount, lapStrokeCount, lapBreathCount, lapTime);
						printf("(%d, %d)\n", startLap, endLap);
						lapBreathCount = 0;
						startLap = i + 1 + 20;
						lapStrokeCount = 0;
					}
					//totalBreathCount += breathsSinceLastFlip;
					breathsSinceLastFlip = 0; 	//restarts breath count
					onlyOneFlip = 100; 	//sets a delay so it doesn't detect another flip turn for 10 seconds
				}



				if (inLap) {
					timeSinceLastStroke++;
				}


				//	detects a lap after a local peak value
				/*if ((currentMax > averageOverOneSecondX) && (onlyOneFlip == 0) && (averageOverOneSecondX >= FLIP_TURN_THRESHOLD) && (inLap))
				{
					flipTurns++; 	//increases flipTurns
					printf("Flip turns = %d\n", flipTurns);
					lapCount = (flipTurns + 1) / 2;
					if (flipTurns%2 == 0) {
						endLap = i;
						lapTime = (float)(endLap - startLap)/10;
						sessionTime += lapTime;
						totalBreathCount += lapBreathCount;
						printf("\nLap %d:\nBreaths = %d\nTime = %.1f seconds\n", lapCount, lapBreathCount, lapTime);
						printf("(%d, %d)\n", startLap, endLap);
						lapBreathCount = 0;
						startLap = i + 1;
					}
					inFlip = 0; 	//inFlip State OFF
					//totalBreathCount += breathsSinceLastFlip;
					breathsSinceLastFlip = 0; 	//restarts breath count
					onlyOneFlip = 100; 	//sets a delay so it doesn't detect another flip turn for 10 seconds
				}
				else
				{
					currentMax = averageOverOneSecondX;		//	updates current accelerometer x max
				}*/

				previousZ = averageOverOneSecondZ;

				if ((averageOverOneSecondGX < GX_START_THRESHOLD) && (inLap) && (flipThresholdCounter > 0)) {
					flipThresholdCounter = 0;
				}

				prevHalfSecondZ = averageOverHalfSecondZ;

				/*if ((i > 1330) && (i < 1370)) {
					printf("AvgX = %d, at %d\n", averageOverHalfSecondX, i);
				}*/

			}


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~End~of~Testing~Algorithm~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


		} else if (calibrating) {


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Calibration~Algorithm~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

			printf("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
			printf("~~~~~~~~~~Calibration~~~~~~~~~~");
			printf("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
			printf("\nThresholds:");

			/*for (i = 0; i < 50; i++) {
				printf("i = %d, x = %d, z = %d\n", i, xAcceleration[i], zAcceleration[i]);
			}*/








			// Calculating when the swimmer starts


			//measure the gx data over half second and set startedSwimming = i when the average is positive for the first time
			for (i = 0; i < csvSize; i++) {
				gx = xGyro[i];
				halfSecondBufferGX[bufferCounterHalf] = gx;
				bufferCounterHalf = (bufferCounterHalf + 1) % 5;

				averageOverHalfSecondGX = (halfSecondBufferGX[0] + halfSecondBufferGX[1] + halfSecondBufferGX[2] + halfSecondBufferGX[3] + halfSecondBufferGX[4])/5;

				if ((startedSwimming == 0) && (averageOverHalfSecondGX > 0)) {
					//printf("\n\nStart Found here at %d\n", i);
					startedSwimming = i;
				}

			}

			// Not needed for the current algorithm
			/*
			// Calculating the average of the first 5 seconds of recording
			for (i = 0; i < 50; i++) {
				tempAdder += xAcceleration[i];
			}
			averageOverFiveSecondsX = tempAdder/50;


			//Clears the twoSecondBufferX to a similar value in order to not have it start unintentionally (if it was zero)

			for (i = 0; i < 20; i++) {
				twoSecondBufferX2[i] = averageOverFiveSecondsX;
			}

			// A two second moving average to detect a large increase over the initial averageOverFiveSecondsX
			for (i = 50; i < csvSize; i++) {
				x = xAcceleration[i];
				twoSecondBufferX2[bufferCounter] = x;
				bufferCounter = (bufferCounter + 1) % 20;

				averageOverTwoSecondsX = (twoSecondBufferX2[0] + twoSecondBufferX2[1] + twoSecondBufferX2[2] + twoSecondBufferX2[3] + twoSecondBufferX2[4] +
						twoSecondBufferX2[5] + twoSecondBufferX2[6] + twoSecondBufferX2[7] + twoSecondBufferX2[8] + twoSecondBufferX2[9] +
						twoSecondBufferX2[10] + twoSecondBufferX2[11] + twoSecondBufferX2[12] + twoSecondBufferX2[13] + twoSecondBufferX2[14] +
						twoSecondBufferX2[15] + twoSecondBufferX2[16] + twoSecondBufferX2[17] + twoSecondBufferX2[18] + twoSecondBufferX2[19])/20;



				if ((averageOverTwoSecondsX >= (averageOverFiveSecondsX + 20)) && (startedSwimming == 0)) {
					startedSwimming = i;
					poolStart = (6*averageOverTwoSecondsX + 2*averageOverFiveSecondsX)/8;
					printf("\nStart       = %d, at %d\n", poolStart, i);
					//printf("two seconds: %d\nfive seconds: %d\n", averageOverTwoSecondsX, averageOverFiveSecondsX);
				}
			}
			*/




			// Calculating when the initial push off ends


			for (i = 0; i < 5; i++) {
				halfSecondBufferX[i] = 127;
			}


			for (i = startedSwimming; i < csvSize; i++) {

				x = xAcceleration[i];

				halfSecondBufferX[bufferCounterHalf] = x;
				bufferCounterHalf = (bufferCounterHalf + 1) % 5;

				averageOverHalfSecondX = (halfSecondBufferX[0] + halfSecondBufferX[1] + halfSecondBufferX[2] + halfSecondBufferX[3] + halfSecondBufferX[4])/5;


				if ((i >= startedSwimming) && (averageOverHalfSecondX < 0) && (endOfPushOff == 0)) {		//or averageOverTwoSecondsX
					endOfPushOff = i - 2;    // -2 to account for the movement of the arm
					//printf("endOfPushOff at %d\n", i - 2);
				}

			}



			// Calculating flip turn threshold


			for (i = 0; i < 20; i++) {
				twoSecondBufferY[i] = yAcceleration[endOfPushOff - i];
			}
			bufferCounter = 0;
			minY = 127;
			for (i = endOfPushOff; i < csvSize; i++) {
				y = yAcceleration[i];
				twoSecondBufferY[bufferCounter] = y;
				bufferCounter = (bufferCounter + 1) % 20;

				averageOverTwoSecondsY = (twoSecondBufferY[0] + twoSecondBufferY[1] + twoSecondBufferY[2] + twoSecondBufferY[3] + twoSecondBufferY[4] +
						twoSecondBufferY[5] + twoSecondBufferY[6] + twoSecondBufferY[7] + twoSecondBufferY[8] + twoSecondBufferY[9] +
						twoSecondBufferY[10] + twoSecondBufferY[11] + twoSecondBufferY[12] + twoSecondBufferY[13] + twoSecondBufferY[14] +
						twoSecondBufferY[15] + twoSecondBufferY[16] + twoSecondBufferY[17] + twoSecondBufferY[18] + twoSecondBufferY[19])/20;

				if (averageOverTwoSecondsY < minY) {
					minY = averageOverTwoSecondsY;
				}
			}
			printf("\n\nFlip Turn   = %d\n", (minY));



			/*
			for (i = 0; i < 20; i++) {
				twoSecondBufferX[i] = -127;
			}
			*/

			/*for (i = 0; i < 30; i++) {
				threeSecondBufferX[i] = -127;
			}

			maxThreeSecondAvgX = -127;
			for (i = startedSwimming + 20; i < csvSize; i++) {

				x = xAcceleration[i];

				threeSecondBufferX[bufferCounter] = x;

				for (j = 0; j < 30; j++) {
					averageOverThreeSecondsX += threeSecondBufferX[j];
				}
				averageOverThreeSecondsX = averageOverThreeSecondsX / 30;
				bufferCounter = (bufferCounter + 1) % 30;

				//printf("average 3 seconds = %d\n", averageOverThreeSecondsX);

				//if ((averageOverTwoSecondsX) > (maxTwoSecondAvgX)) {
					//maxTwoSecondAvgX = averageOverTwoSecondsX;
					//printf("New max at i = %d\n", i);
				//}

				if ((averageOverThreeSecondsX) > (maxThreeSecondAvgX)) {
					maxThreeSecondAvgX = averageOverThreeSecondsX;
					//printf("New max at i = %d\n", i);
				}

			}
			printf("Flip Turn   = %d\n", (maxThreeSecondAvgX));
			*/










			// Calculating a breath (look into a big drop or big increase directly afterwards to show its a breath)
			/*
			tempZHighAvg = (zAcceleration[startedSwimming] + zAcceleration[startedSwimming + 1] + zAcceleration[startedSwimming + 2] +
					zAcceleration[startedSwimming + 3] + zAcceleration[startedSwimming + 4] + zAcceleration[startedSwimming + 5]
					+ zAcceleration[startedSwimming + 6] + zAcceleration[startedSwimming + 7] + zAcceleration[startedSwimming + 8]
					+ zAcceleration[startedSwimming + 9])/10;

			tempBreathMin = (tempZHighAvg + averageOverFiveSecondsX)/2;
			bufferFiveSecondCounter = 0;
			stoppingThresholdCounter = 0;

			for (i = 0; i < 50; i++) {
				fiveSecondBufferZ[i] = (uint8_t)tempZHighAvg;
			}

			for (i = startedSwimming + 50; i < csvSize; i++) {
				z = zAcceleration[i];
				x = xAcceleration[i];


				fiveSecondBufferZ[bufferCounter] = x;

				for (j = 0; j < 50; j++) {
					averageOverFiveSecondsX += fiveSecondBufferZ[j];
				}
				averageOverFiveSecondsX = averageOverFiveSecondsX / 30;
				bufferCounter = (bufferCounter + 1) % 50;



				halfSecondBufferX[bufferCounterHalf] = z;
				bufferCounterHalf = (bufferCounterHalf + 1) % 5;



				averageOverHalfSecondX = (halfSecondBufferX[0] + halfSecondBufferX[1] + halfSecondBufferX[2] + halfSecondBufferX[3] + halfSecondBufferX[4])/5;


				if (i < 745) {
					printf("avg half sec = %d, tempZHighAvg = %d, i=%d\n", averageOverHalfSecondX, tempZHighAvg, i);
				}

				if ((breathMin == 127) && (averageOverHalfSecondX < tempBreathMin)) {
					tempBreathMin = averageOverHalfSecondX;
					tempBreathMinHit = 1;
					breathMinPoint = i;
					printf("hit\n");
				}

				if ((tempBreathMinHit) && (averageOverHalfSecondX >= (tempZHighAvg - 5))) {
					breathMin = tempBreathMin;
					breathMinAvg = (zAcceleration[breathMinPoint - 2] + zAcceleration[breathMinPoint - 1] + zAcceleration[breathMinPoint]
							+ zAcceleration[breathMinPoint + 1] + zAcceleration[breathMinPoint + 2])/5;
					breathThreshold = breathMinAvg;
					//printf("breathMinAvg = %d\n", breathMinAvg);
					//breathThreshold = (4*tempZHighAvg + 3*averageOverFiveSecondsX)/7;
					printf("Breath      = %d, at %d\n", breathThreshold, i);
					tempBreathMinHit = 0;
					breathFound = i;
				}

			}
			*/

			//Calculating the high breath threshold

			for (i = 0; i < 10; i++) {
				oneSecondBufferZ[i] = 127;
			}

			for (i = endOfPushOff; i < csvSize; i++) {
				z = zAcceleration[i];
				oneSecondBufferZ[bufferCounter] = z;
				bufferCounter = (bufferCounter + 1) % 10;

				averageOverOneSecondZ = (oneSecondBufferZ[0] + oneSecondBufferZ[1] + oneSecondBufferZ[2] + oneSecondBufferZ[3] + oneSecondBufferZ[4] +
						oneSecondBufferZ[5] + oneSecondBufferZ[6] + oneSecondBufferZ[7] + oneSecondBufferZ[8] + oneSecondBufferZ[9])/10;

				if (averageOverOneSecondZ < minZ) {
					// minZ is where flip turns occur
					minZ = averageOverOneSecondZ;
				}
			}

			// reset the array
			for (i = 0; i < 10; i++) {
				oneSecondBufferZ[i] = 127;
			}

			// The
			startValueOneSecondZ = ((zAcceleration[endOfPushOff] + zAcceleration[endOfPushOff + 1] + zAcceleration[endOfPushOff + 2] + zAcceleration[endOfPushOff + 3] + zAcceleration[endOfPushOff + 4] +
					zAcceleration[endOfPushOff + 5] + zAcceleration[endOfPushOff + 6] + zAcceleration[endOfPushOff + 7] + zAcceleration[endOfPushOff + 8] + zAcceleration[endOfPushOff + 9])/10);

			//printf("Start Value = %d\n", startValueOneSecondZ);

			tempBreathMin = startValueOneSecondZ;
			breathMinLocation = endOfPushOff;
			flipTurnHit = 0;


			for (i = endOfPushOff + 10; i < csvSize; i++) {
				z = zAcceleration[i];
				oneSecondBufferZ[bufferCounter] = z;
				bufferCounter = (bufferCounter + 1) % 10;

				averageOverOneSecondZ = (oneSecondBufferZ[0] + oneSecondBufferZ[1] + oneSecondBufferZ[2] + oneSecondBufferZ[3] + oneSecondBufferZ[4] +
						oneSecondBufferZ[5] + oneSecondBufferZ[6] + oneSecondBufferZ[7] + oneSecondBufferZ[8] + oneSecondBufferZ[9])/10;

				/*if (i < 850) {
					printf("one sec avg = %d, breathMin = %d, i = %d\n", averageOverOneSecondZ, breathMin, i);
				}*/

				if ((averageOverOneSecondZ < startValueOneSecondZ) && (flipTurnHit == 0)) {
					belowStartValue = 1;
					//printf("Below at %d\n", i);
					if (averageOverOneSecondZ < tempBreathMin) {
						tempBreathMin = averageOverOneSecondZ;
						tempBreathMinLocation = i;
					}
					if (averageOverOneSecondZ < (minZ + 10)) {
						//printf("flipTurnHit at %d\n", i);
						flipTurnHit = i;
					}
				} else {
					belowStartValue = 0;
					//printf("Above at %d\n", i);
				}

				if ((belowStartValue == 0) && (flipTurnHit == 0)) {
					if (tempBreathMin < breathMin) {
						breathMin = tempBreathMin;
						breathMinLocation = tempBreathMinLocation;
					}
				}
			}


			// Find the corresponding value in the x data
			for (i = 0; i < 10; i++) {
				oneSecondBufferX[i] = xAcceleration[breathMinLocation - i];
			}

			breathHighThreshold = (oneSecondBufferX[0] + oneSecondBufferX[1] + oneSecondBufferX[2] + oneSecondBufferX[3] + oneSecondBufferX[4] +
							oneSecondBufferX[5] + oneSecondBufferX[6] + oneSecondBufferX[7] + oneSecondBufferX[8] +  + oneSecondBufferX[9])/10;

			if (breathHighThreshold > 0) {
				highBreathFoundFirst = 1;
				printf("Breath High = %d, at %d\n", breathHighThreshold, breathMinLocation);
			} else {
				highBreathFoundFirst = 0;
				//breathHighThreshold += 5;
				printf("v Breath Low = %d, at %d\n", breathHighThreshold, breathMinLocation);
			}









			// Calculating stroke threshold

			/*for (i = 0; i < 5; i++) {
				halfSecondBufferX[i] = xAcceleration[breathMinLocation + i];
			}*/

			// The +5 is do accommodate between half second and one second smoothing
			for (i = 0; i < 10; i++) {
				oneSecondBufferX[i] = xAcceleration[breathMinLocation + i + 5];
			}

			strokeLowCounter = 0;
			strokeLowFound = 0;
			strokeLow = 0;
			strokeHighCounter = 0;
			strokeHighFound = 0;
			strokeHigh = 0;
			bufferCounter = 0;
			for (i = breathMinLocation; i < csvSize; i++) {
				// Use a moving average of 5 points to smooth out the data
				x = xAcceleration[i];
				/*halfSecondBufferX[bufferCounter] = x;
				bufferCounter = (bufferCounter + 1) % 5;

				averageOverHalfSecondX = (oneSecondBufferX[0] + halfSecondBufferX[1] + halfSecondBufferX[2] + halfSecondBufferX[3] + halfSecondBufferX[4])/5;
				//printf("AverageOverHalfSecond = %d\n", averageOverHalfSecondX);
				*/

				oneSecondBufferX[bufferCounter] = x;
				bufferCounter = (bufferCounter + 1) % 10;

				averageOverOneSecondX = (oneSecondBufferX[0] + oneSecondBufferX[1] + oneSecondBufferX[2] + oneSecondBufferX[3] + oneSecondBufferX[4] +
						oneSecondBufferX[5] + oneSecondBufferX[6] + oneSecondBufferX[7] + oneSecondBufferX[8] +  + oneSecondBufferX[9])/10;

				if (highBreathFoundFirst) {
					if (strokeLow == 0) {
						strokeLow = averageOverOneSecondX;
					}

					if ((averageOverOneSecondX <= strokeLow) && (!strokeLowFound)) {
						strokeLow = averageOverOneSecondX;
						strokeLowCounter = 0;
					} else {
						strokeLowCounter++;
					}

					if ((strokeLowCounter >= 5) && (!strokeLowFound)) {
						//printf("Stroke Low  = %d, at %d\n", (strokeLow + 3), i - 5);
						strokeLowFound = i;
					}

					if ((strokeHigh == 0) && (strokeLowFound)) {
						strokeHigh = averageOverOneSecondX;
					}

					if ((averageOverOneSecondX >= strokeHigh) && (strokeLowFound) && (!strokeHighFound)) {
						strokeHigh = averageOverOneSecondX;
						strokeHighCounter = 0;
					} else {
						strokeHighCounter++;
					}

					if ((strokeHighCounter >= 5) && (!strokeHighFound) && (strokeLowFound)) {
						//printf("Stroke High = %d, at %d (%d)\n", strokeHigh - 3, i - 5, (strokeHigh + strokeLow)/2);
						strokeHighFound = i;
					}
				} else {
					if (strokeHigh == 0) {
						strokeHigh = averageOverOneSecondX;
					}

					if ((averageOverOneSecondX >= strokeHigh) && (!strokeHighFound)) {
						strokeHigh = averageOverOneSecondX;
						strokeHighCounter = 0;
					} else {
						strokeHighCounter++;
					}

					if ((strokeHighCounter >= 5) && (!strokeHighFound)) {
						//printf("v Stroke High  = %d, at %d\n", (strokeHigh - 3), i - 5);
						strokeHighFound = i;
					}

					if ((strokeLow == 0) && (strokeHighFound)) {
						strokeLow = averageOverOneSecondX;
					}

					if ((averageOverOneSecondX <= strokeLow) && (strokeHighFound) && (!strokeLowFound)) {
						strokeLow = averageOverOneSecondX;
						strokeLowCounter = 0;
					} else {
						strokeLowCounter++;
					}

					if ((strokeLowCounter >= 5) && (!strokeLowFound) && (strokeHighFound)) {
						//printf("^ Stroke Low = %d, at %d ^(%d)\n", strokeLow + 3, i - 5, (strokeHigh + strokeLow)/2);
						strokeLowFound = i;
					}
				}
			}
			if (highBreathFoundFirst) {
				printf("Breath Low  = %d\n", ((-1) * breathHighThreshold) + 5);
				printf("Interval    = %d\n", strokeHighFound - strokeLowFound);
			} else {
				printf("^ Breath High = %d\n", ((-1) * breathHighThreshold) + 5);
				printf("Interval    = %d\n", strokeLowFound - strokeHighFound);
			}
			//printf("Stroke low = -1\nStroke high = 1\n");








			// Calculating the end of the swim


			for (i = 0; i < 10; i++) {
				oneSecondBufferGX[i] = xGyro[startedSwimming + 50 + i];
			}

			minGX = 127;

			for (i = startedSwimming + 50; i < startedSwimming + 125; i++) {
				gx = xGyro[i];

				oneSecondBufferGX[bufferCounter] = gx;
				bufferCounter = (bufferCounter + 1) % 10;

				averageOverOneSecondGX = (oneSecondBufferGX[0] + oneSecondBufferGX[1] + oneSecondBufferGX[2] + oneSecondBufferGX[3] + oneSecondBufferGX[4] +
									oneSecondBufferGX[5] + oneSecondBufferGX[6] + oneSecondBufferGX[7] + oneSecondBufferGX[8] +  + oneSecondBufferGX[9])/10;

				if (averageOverOneSecondGX < minGX) {
					minGX = averageOverOneSecondGX;
				}
			}
			stoppingThreshold = minGX - 5;
			printf("Stopping    = %d\n", stoppingThreshold);









			// Old stopping algorithm
			/*
			stoppingThreshold = 0;

			for (i = startedSwimming + 50; i < csvSize - 50; i++) {			// i = STARTEDSWIMMING + 50
				z = xAcceleration[i];

				for (bufferFiveSecondCounter = 0; bufferFiveSecondCounter < 50; bufferFiveSecondCounter++) {
					fiveSecondBufferZ[bufferFiveSecondCounter] = zAcceleration[i + bufferFiveSecondCounter];
				}



				for (thresholdLoop = 0; thresholdLoop <= 39; thresholdLoop++) {
					//if ((fiveSecondBufferZ[thresholdLoop] >= (averageOverFiveSecondsX - 10)) && (fiveSecondBufferZ[thresholdLoop] <= (averageOverFiveSecondsX + 10))) {
					//	stoppingThresholdCounter++;
					//}
					for (bufferCounter = 0; bufferCounter < 10; bufferCounter++) {
						oneSecondBufferZ[bufferCounter] = zAcceleration[i + bufferCounter + thresholdLoop];
						//printf("%d,", zAcceleration[i + bufferCounter + thresholdLoop]);
					}
					oneSecondMin = 127;
					oneSecondMax = -127;
					for (bufferCounter = 0; bufferCounter < 10; bufferCounter++) {
						if (oneSecondBufferZ[bufferCounter] <= oneSecondMin) {
							oneSecondMin = oneSecondBufferZ[bufferCounter];
						} else if (oneSecondBufferZ[bufferCounter] >= (oneSecondMax)) {
							oneSecondMax = oneSecondBufferZ[bufferCounter];
						}
					}
					peakInData = oneSecondMax - oneSecondMin;
					//printf("\nMax = %d, min = %d, Max - min = %d\n", oneSecondMax, oneSecondMin, peakInData);
					if ((peakInData <= 20) && (oneSecondMin != 127) && (oneSecondMax != (-127))) {
						nonPeakCounter++;
						if (startedSwimming) {
							//printf("nonPeakCounter++ = %d\n\n", nonPeakCounter);
						}
					}

				}
				//	sets to not swimming if average z acceleration if between the thresholds 3.5 seconds out of 5
				if ((nonPeakCounter >= 35) && (startedSwimming)) {
					//printf("Swim end = %d\n", i - 45);
					//printf("nonPeakCounter = %d\n", nonPeakCounter);
					for (stoppingThresholdCounter = 0; stoppingThresholdCounter < 50; stoppingThresholdCounter++) {
						stoppingThreshold += fiveSecondBufferZ[stoppingThresholdCounter];
						//printf("%d\n", fiveSecondBufferZ[stoppingThresholdCounter]);
					}
					stoppingThreshold = (stoppingThreshold/50) - 15;
					printf("Stopping    = %d, at %d\n", stoppingThreshold, i + 25);
					startedSwimming = 0;
				}
				stoppingThresholdCounter = 0;
				nonPeakCounter = 0;


			}*/




//~~~~~~~~~~~~~~~~~~~~~~~~~~~~End~of~Calibration~Algorithm~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		} // end of if statement


	} // end of if statement

	return 0;
}
